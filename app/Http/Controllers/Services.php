<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Services extends Controller{

    public function servicios(){
        return view("services");
    }

    public function freeservice(){
        return view("freeservice");
    }

    public function premiumservice(){
        return view("premiumservice");
    }
}