<?php

use App\Http\Controllers\Services;
use App\Http\Controllers\Contact;
use App\Http\Controllers\Us;
use App\Http\Controllers\Home;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/weather', [Home::class,'home']);
Route::get('/weather/services', [Services::class,'servicios']);
Route::get('/weather/services/freeservice', [Services::class,'freeservice']);
Route::get('/weather/services/premiumservice', [Services::class,'premiumservice']);
Route::get('/weather/us', [Us::class,'nosotros']);
Route::get('/weather/contact', [Contact::class,'contacto']);
Route::get('/weather/contact/saveform', [Contact::class,'saveform']);
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
