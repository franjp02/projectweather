@extends('layouts.app')

@section('content')

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ asset('img/icon.png') }}">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
    <title>Inicio</title>

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/estilos.css') }}" rel="stylesheet">
    <link href="{{ asset('css/carousel.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('themify-icons/themify-icons.css') }}">
    <link href="{{ asset('css/menu.css') }}" rel="stylesheet">

  <div id="menuToggle"> 
  
    <input type="checkbox" />
    <span></span>
    <span></span>
    <span></span>
    
    <ul id="menu">
    <li class="nav-item">
       <a href="{{ url('/weather') }}">
          <img src="{{ asset('img/icon.png') }}" width="40px" >
      </a>
    </li>
      <a href="{{ url('/weather') }}"><li>Home</li></a>
      <a href="{{ url('/weather/services') }}"><li>Servicios</li></a>
      <li class="dropdown">
        <a class="dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown">Nosotros</a>
          <div class="dropdown-menu" aria-labelledby="dropdown01">
            <a class="dropdown-item" href="{{ url('/weather/us') }}">El Equipo</a>
            <a class="dropdown-item" href="{{ url('/weather/us') }}">Misión</a>                
          </div>
      </li>                      
      <a href="{{ url('/weather/contact') }}"><li>Contacto</li></a>
    </ul>
  </div>

  </head>
  <body>



     <div class="container ">
   
      
      <main role="main">

        <div id="myCarousel" class="carousel slide rounded mb-4" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
          </ol>
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img class="first-slide" src="{{ asset('img/1.jpg')}} " alt="First slide" width="720x800">
              <div class="container">
                <div class="carousel-caption text-left">
                  <h1 class="tcs"><strong>¡Conoce nuestros servicios!</strong></h1>
                  <p class="tcs"></p>
                  <p><a class="btn btn-lg btn-primary" href="{{ url('/weather/services') }}" target="_blank" role="button">Leer más</a></p>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <img class="second-slide" src="{{ asset('img/2.jpg')}} " alt="Second slide">
              <div class="container">
                <div class="carousel-caption">
                  <h1 class="tcs"><strong>¡Servicio excelente para saber el tiempo!</strong></h1>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <img class="third-slide" src="{{ asset('img/3.jpg')}}" alt="Third slide" width="100px">
            </div>
          </div>
          <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Anterior</span>
          </a>
          <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Siguiente</span>
          </a>
        </div>
        <div class="row">
          <div class="col-lg-4">
            <h2>Servicios</h2>
            <p>Informate de nuestros 2 servicios,con registro y sin él</p>
            <p><a class="btn btn-primary" href="{{ url('/weather/services') }}" target="_blank" role="button">Ver detalles &raquo;</a></p>
          </div>
          <div class="col-lg-4">
            <h2>Conócenos</h2>
            <p>Somos una pequeña empresa en continuo crecimiento</p>
            <p><a class="btn btn-primary" href="{{ url('/weather/us') }}" target="_blank" role="button">Leer más &raquo;</a></p>
          </div>
        </div> 
      </main>

      </div>

      <div class="footrwelcome">
      <footer>
        <div class="container mt-4">
          <p>&copy; <script>document.write(new Date().getFullYear())</script> Desarrollado por <a>Francisco Jiménez</a> </p>
          <ul class="list-inline d-inline">
            <li class="list-inline-item mx-0"><a class="d-inline-block p-2 text-color" href="#"><i class="ti-facebook"></i></a></li>
            <li class="list-inline-item mx-0"><a class="d-inline-block p-2 text-color" href="#"><i class="ti-twitter-alt"></i></a></li>
            <li class="list-inline-item mx-0"><a class="d-inline-block p-2 text-color" href="#"><i class="ti-linkedin"></i></a></li>
            <li class="list-inline-item mx-0"><a class="d-inline-block p-2 text-color" href="#"><i class="ti-instagram"></i></a></li>
          </ul>
        </div>
      </footer>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-slim.min.js"><\/script>')</script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
@endsection