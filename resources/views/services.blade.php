@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ asset('img/icon.png') }}">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
    <title>Servicios</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset('css/estilos.css') }}" rel="stylesheet">
    <link href="{{ asset('css/carousel.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('themify-icons/themify-icons.css') }}">
    <link href="{{ asset('css/menu.css') }}" rel="stylesheet">
    
  <div id="menuToggle"> 
  
    <input type="checkbox" />
    <span></span>
    <span></span>
    <span></span>
    
    <ul id="menu">
    <li class="nav-item">
       <a href="{{ url('/weather') }}">
          <img src="{{ asset('img/icon.png') }}" width="40px" >
      </a>
    </li>
      <a href="{{ url('/weather') }}"><li>Home</li></a>
      <a href="{{ url('/weather/services') }}"><li>Servicios</li></a>
      <li class="dropdown">
        <a class="dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown">Nosotros</a>
          <div class="dropdown-menu" aria-labelledby="dropdown01">
            <a class="dropdown-item" href="{{ url('/weather/us') }}">El Equipo</a>
            <a class="dropdown-item" href="{{ url('/weather/us') }}">Misión</a>                
          </div>
      </li>                      
      <a href="{{ url('/weather/contact') }}"><li>Contacto</li></a>
    </ul>
  </div>
  </head>
<body>
    
<div class="container">

<header class="masthead">


</header>

<main role="main">

   <div class="row">
      <div class="col-md-4">
        <div class="card mb-4 box-shadow " style="background-color: #7777; font-family:font-italic;" >
          <img class="card-img-top" src="{{ asset('img/6.jpg')}}" alt="Servicio Gratuito">
          <div class="card-body">
            <h1>FreeWeather</h1>
            <p class="card-text">Se ofrece un servicio gratuito sin la necesidad de registrarse pero con limitaciones al acceso de datos meteorólogicos </p>
            <br>
            <p class="card-text">Introduce una ciudad: </p>
            <div class="d-flex justify-content-between align-items-center">
     
              <div class="btn-group">
              <form  class="flex-form" action="{{ url('/weather/services/freeservice') }}" style="width: 1px;" method="GET">
                <label for="from">
                 <i class="ion-location"></i>
                </label>
                <input type="text" id="keywords" name="keywords" size="30" maxlength="30">
                <input type="submit" name="search" id="search" value="Buscar">
              </form>
              </div>
            </div>
          </div>
        </div>
      </div>
@auth
      <div class="col-md-4">
        <div class="card mb-4 box-shadow" style="background-color: #7777; font-family:font-italic;">
          <img class="card-img-top" src="{{ asset('img/7.jpg')}}" alt="Servicio Premium">
          <div class="card-body">
            <h1>FullWeather</h1>
            <p class="card-text">Se ofrece un servicio premium a los usuarios registrados,con acceso a una gran cantidad de datos meteorólogicos</p>
            <br>
            <p class="card-text">Introduce una ciudad: </p>
            <div class="d-flex justify-content-between align-items-center">
              <div class="btn-group">
              <form  class="flex-form" action="{{ url('/weather/services/premiumservice') }}" style="width: 10px;" method="GET">
                <label for="from">
                 <i class="ion-location"></i>
                </label>
                <input type="text" id="keywords" name="keywords" size="30" maxlength="30">
                <input type="submit" name="search" id="search" value="Buscar">
              </form>
              </div>
            </div>
          </div>
        </div>
      </div>
@endauth
</main>

</div>

      <div class="footrServices">
      <footer>
        <div class="container mt-4">
          <p>&copy; <script>document.write(new Date().getFullYear())</script> Desarrollado por <a>Francisco Jiménez</a> </p>
          <ul class="list-inline d-inline">
            <li class="list-inline-item mx-0"><a class="d-inline-block p-2 text-color" href="#"><i class="ti-facebook"></i></a></li>
            <li class="list-inline-item mx-0"><a class="d-inline-block p-2 text-color" href="#"><i class="ti-twitter-alt"></i></a></li>
            <li class="list-inline-item mx-0"><a class="d-inline-block p-2 text-color" href="#"><i class="ti-linkedin"></i></a></li>
            <li class="list-inline-item mx-0"><a class="d-inline-block p-2 text-color" href="#"><i class="ti-instagram"></i></a></li>
          </ul>
        </div>
      </footer>
    </div>
    
    <script src="{{ asset('https://code.jquery.com/jquery-3.2.1.slim.min.js')}}"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-slim.min.js"><\/script>')</script>
    <script>window.jQuery || document.write('<script src="js/jquery-slim.min.js"><\/script>')</script>
    <script src="{{ asset('js/popper.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap.min.js')}}"></script>
  </body>
</html>
@endsection