@extends('layouts.app')

@section('content')
<?php

  $cityId = $_GET['keywords'];

 

$apiKey = "6b3069baff99742d5a925346256a4388";
$googleApiUrl = "http://api.openweathermap.org/data/2.5/weather?q=" . $cityId . "&lang=es&units=metric&APPID=" . $apiKey;

$ch = curl_init();

curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_URL, $googleApiUrl);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_VERBOSE, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
$response = curl_exec($ch);

curl_close($ch);
$data = json_decode($response);
$currentTime = time();
?>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ asset('img/icon.png') }}">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
    <title>Servicio Gratuito</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset('css/estilos.css') }}" rel="stylesheet">
    <link href="{{ asset('css/carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('css/menu.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('themify-icons/themify-icons.css') }}">

  <div id="menuToggle"> 
  
    <input type="checkbox" />
    <span></span>
    <span></span>
    <span></span>
    
    <ul id="menu">
    <li class="nav-item">
       <a href="{{ url('/weather') }}">
       <img src="{{ asset('img/icon.png') }}" width="40px" >
      </a>
    </li>
      <a href="{{ url('/weather') }}"><li>Home</li></a>
      <a href="{{ url('/weather/services') }}"><li>Servicios</li></a>
      <li class="dropdown">
        <a class="dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown">Nosotros</a>
          <div class="dropdown-menu" aria-labelledby="dropdown01">
            <a class="dropdown-item" href="{{ url('/weather/us') }}">El Equipo</a>
            <a class="dropdown-item" href="{{ url('/weather/us') }}">Misión</a>                
          </div>
      </li>                      
      <a href="{{ url('/weather/contact') }}"><li>Contacto</li></a>
    </ul>
  </div>

  <style>
.bck1{
  background-image: url("{{ asset('img/gif.gif') }}");
  width: auto; 
  height: auto;
  
}
.bck2{
  background-image: url("{{ asset('img/gif2.gif') }}");

}
.bck3{
  background-image: url("{{ asset('img/gif3.gif') }}");
}
.bck4{
  background-image: url("{{ asset('img/gif4.gif') }}");
}
.bck5{
  background-image: url("{{ asset('img/gif5.gif') }}");
}
.bck6{
  background-image: url("{{ asset('img/gif6.gif') }}");
}

</style>
</head>

<body >

<div class="container">
<form  class="flex-form search_" action="{{ url('/weather/services/freeservice') }}" method="GET">
        <label for="from">
          <i class="ion-location"></i>
        </label>
    <input type="text" id="keywords" name="keywords" size="30" maxlength="30">
    <input type="submit" name="search" id="search" value="Buscar">
</form>
<h1 class="tittleweather" style="color:white;position:relative;left:400px;">Servicio Premium</h1>
<div class="report-container box">
        <h1 class="tittleweather">Tiempo en <?php echo $data->name; ?></h1>

      <div class="time">  
            <span class="country" >País actual: <?php echo $data->sys->country; ?></span>
            <div><?php echo date("l g:i a", $currentTime); ?></div>
            <div><?php echo date("jS F, Y",$currentTime); ?></div>
          <?php if ($data->weather[0]->description=='algo de nubes'||$data->weather[0]->description=='nubes dispersas') {
             echo '<body class="bck1">';
          } elseif ($data->weather[0]->description=='cielo claro') {
            echo '<body class="bck2">';
          }
           elseif ($data->weather[0]->description=='nubes'||$data->weather[0]->description=='muy nuboso') {
            echo '<body class="bck3">';
          }
           elseif ($data->weather[0]->description=='lluvia ligera' || 'lluvia moderada') {
            echo '<body class="bck4">';
          }
           elseif ($data->weather[0]->description=='niebla') {
            echo '<body class="bck5">';
          }
           elseif ($data->weather[0]->description=='chubasco de ligera intensidad') {
            echo '<body class="bck6">';
          }
          
          ?>
            <div><?php echo ucwords($data->weather[0]->description); ?></div> 
      </div>
          
      <div class="weather-forecast">
            <img
                src="http://openweathermap.org/img/w/<?php echo $data->weather[0]->icon; ?>.png"
                class="weather-icon" />
                <br>
                <span class="max-temperature">Temperatura Máxima:<?php echo $data->main->temp_max; ?>°C </span>
                <br>
                <span class="min-temperature">Temperatura Mínima:<?php echo $data->main->temp_min; ?>°C </span>
                <br>
                <span class="temperature" >Temperatura Actual: <?php echo $data->main->temp; ?>°C </span>
                <br>
                <span class="visibility" >Visibilidad: <?php echo $data->visibility; ?></span>

      </div>
      <div class="time">
            <div>Húmedad: <?php echo $data->main->humidity; ?> %</div>
            <div>viento: <?php echo $data->wind->speed; ?> km/h</div>
      </div>
</div>


</div>
      <div class="footr2">
      <footer>
        <div class="container mt-4">
          <p>&copy; <script>document.write(new Date().getFullYear())</script> Desarrollado por <a>Francisco Jiménez</a> </p>
          <ul class="list-inline d-inline">
            <li class="list-inline-item mx-0"><a class="d-inline-block p-2 text-color" href="#"><i class="ti-facebook"></i></a></li>
            <li class="list-inline-item mx-0"><a class="d-inline-block p-2 text-color" href="#"><i class="ti-twitter-alt"></i></a></li>
            <li class="list-inline-item mx-0"><a class="d-inline-block p-2 text-color" href="#"><i class="ti-linkedin"></i></a></li>
            <li class="list-inline-item mx-0"><a class="d-inline-block p-2 text-color" href="#"><i class="ti-instagram"></i></a></li>
          </ul>
        </div>
      </footer>
    </div>
    
    <script src="{{ asset('https://code.jquery.com/jquery-3.2.1.slim.min.js')}}"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-slim.min.js"><\/script>')</script>
    <script>window.jQuery || document.write('<script src="js/jquery-slim.min.js"><\/script>')</script>
    <script src="{{ asset('js/popper.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap.min.js')}}"></script>
  </body>
</html>
@endsection