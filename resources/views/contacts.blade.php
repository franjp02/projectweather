@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ asset('img/icon.png') }}">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
    <title>Servicios</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset('css/estilos.css') }}" rel="stylesheet">
    <link href="{{ asset('css/carousel.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('themify-icons/themify-icons.css') }}">
    <link href="{{ asset('css/menu.css') }}" rel="stylesheet">

  <div id="menuToggle"> 
  
    <input type="checkbox" />
    <span></span>
    <span></span>
    <span></span>
    
    <ul id="menu">
    <li class="nav-item">
       <a href="{{ url('/weather') }}">
       <img src="{{ asset('img/icon.png') }}" width="40px">
      </a>
    </li>
      <a href="{{ url('/weather') }}"><li>Home</li></a>
      <a href="{{ url('/weather/services') }}"><li>Servicios</li></a>
      <li class="dropdown">
        <a class="dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown">Nosotros</a>
          <div class="dropdown-menu" aria-labelledby="dropdown01">
            <a class="dropdown-item" href="{{ url('/weather/us') }}">El Equipo</a>
            <a class="dropdown-item" href="{{ url('/weather/us') }}">Misión</a>                
          </div>
      </li>                      
      <a href="{{ url('/weather/contact') }}"><li>Contacto</li></a>
    </ul>
  </div>
  </head>
  <body>
    
  
<div class="container">


      <main role="main" class="container">
        <div class="row">
          <div class="col-12 col-md-12">           
            <p class="float-right d-md-none">
              <button type="button" class="btn btn-primary btn-sm" data-toggle="offcanvas">Toggle nav</button>
            </p>           
            <div class="gmcont">
              <div class="embed-responsive embed-responsive-16by9">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1247.0080683339538!2d-15.423191227626294!3d27.869077818953073!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xc4098db9d30caef%3A0x553f8124b1bf8a58!2s35260%20Ag%C3%BCimes%2C%20Las%20Palmas!5e0!3m2!1ses!2ses!4v1643842085870!5m2!1ses!2ses" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>            
              </div>
            </div>
          </div>       
        </div>
        <hr>

        <div class="row">

          <div class="col-12 col-md-6">

            <h4 class="font-italic">Formulario</h4>
            
            <div class="frmcont">

              <form action="{{ url('/weather/contact/saveform') }}" onsubmit="save()" method="GET">
                
                <div class="form-group">
                  <label for="nya">Nombres y Apellidos</label>
                  <input type="text" class="form-control" id="name_surname" name="name_surname" aria-describedby="nyalHelp" placeholder="Ingresa Nombres y Apellidos" required>
                </div>

                <div class="form-group">
                  <label for="mail">Email</label>
                  <input type="email" class="form-control" id="email" name="email" aria-describedby="mailHelp" placeholder="Ingresa tu Email" required>
                </div>
                <div class="form-group">
                  <label for="pais">Asunto</label>
                  <select class="form-control" id="issue" name="issue" required>
                    <option value="">--Selecciona--</option>
                    <option>Consulta</option>
                    <option>Problemas con el servicio</option>
                    <option>Mejoras</option>
                    <option>Otro</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="nya">Comentario</label>
                  <textarea type="text" class="form-control" id="comment" name="comment" placeholder="Ingresa un comentario" required></textarea>
                </div>                     
                <button type="submit" class="btn btn-primary" >Enviar</button>
              </form>
                
            </div>

          </div>

          <div class="col-12 col-md-3 offset-md-1">
            <h4 class="font-italic">Datos</h4>         
            <div class="">
              <img src="{{ asset('svg/si-glyph-pin-location-2.svg') }}" class="iconosvg" /><strong> Ubicación:</strong> Gran Canaria
              <br>
              <img src="{{ asset('svg/si-glyph-phone-number.svg') }} " class="iconosvg" /><strong> Teléfono:</strong> +34 663 303 324
              <br>
              <img src="{{ asset('svg/si-glyph-mail.svg') }} " class="iconosvg" /> <strong> Correo electrónico:</strong> correo@correo.com     
            </div>
          </div>        

        </div>

      </main>

      <br>
     
     </div>

</div>

      <div class="footr">
      <footer>
        <div class="container mt-4">
          <p>&copy; <script>document.write(new Date().getFullYear())</script> Desarrollado por <a>Francisco Jiménez</a> </p>
          <ul class="list-inline d-inline">
            <li class="list-inline-item mx-0"><a class="d-inline-block p-2 text-color" href="#"><i class="ti-facebook"></i></a></li>
            <li class="list-inline-item mx-0"><a class="d-inline-block p-2 text-color" href="#"><i class="ti-twitter-alt"></i></a></li>
            <li class="list-inline-item mx-0"><a class="d-inline-block p-2 text-color" href="#"><i class="ti-linkedin"></i></a></li>
            <li class="list-inline-item mx-0"><a class="d-inline-block p-2 text-color" href="#"><i class="ti-instagram"></i></a></li>
          </ul>
        </div>
      </footer>
    </div>
    
    <script src="{{ asset('https://code.jquery.com/jquery-3.2.1.slim.min.js')}}"></script>
    <script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js')}}"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-slim.min.js"><\/script>')</script>
    <script>window.jQuery || document.write('<script src="js/jquery-slim.min.js"><\/script>')</script>
    <script src="{{ asset('js/popper.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap.min.js')}}"></script>
    <script type="text/javascript">function save() {bootbox.alert("Formulario enviado correctamente"),4000 }</script>
             
  </body>
</html>
@endsection
