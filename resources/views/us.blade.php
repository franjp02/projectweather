@extends('layouts.app')

@section('content')
<!doctype html>
<html lang="es">
  <head>
  
  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ asset('img/icon.png') }}">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
    <title>Nosotros</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset('css/estilos.css') }}" rel="stylesheet">
    <link href="{{ asset('css/carousel.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('themify-icons/themify-icons.css') }}">
    <link href="{{ asset('css/menu.css') }}" rel="stylesheet">
    
  <div id="menuToggle"> 
  
  <input type="checkbox" />
  <span></span>
  <span></span>
  <span></span>
  
  <ul id="menu">
  <li class="nav-item">
     <a href="{{ url('/weather') }}">
     <img src="{{ asset('img/icon.png') }}" width="40px" >
    </a>
  </li>
    <a href="{{ url('/weather') }}"><li>Home</li></a>
    <a href="{{ url('/weather/services') }}"><li>Servicios</li></a>                      
    <a href="{{ url('/weather/contact') }}"><li>Contacto</li></a>
  </ul>
</div>

  <body >

     <div class="container">

      <main role="main" class="container">

        <div class="row row-offcanvas row-offcanvas-right">

          <div class="col-12 col-md-9">
            <div class="imgnos">
              <img src="{{ asset('img/4.jpg') }}" class="img-fluid" width="435px">
              <img src="{{ asset('img/5.jpg') }}" width="435px" class="img-fluid">
            </div>
            <div class="nosotroscentr">
            <div class="row mt-4">
              <div class="col-6 col-lg-5">
                <h2 style="font-family:font-italic;">El Equipo</h2>
                <p>Esta equipo nace en 2022. Es la web del tiempo con mayor potencial.Poseemos 1 sede en España.</p>
              </div>
              <div class="col-6 col-lg-5">
                <h2 style="font-family:font-italic;">Misión</h2>
                <p>Es la de una organización que pueden ofrecer una amplia gama de soluciones que ofrece a nuestros clientes con la mayor estabilidad.</p>
              </div>
            </div>

            </div>
          </div>

      </main>

    </div>


        <div class="footr2">
          <footer>
            <div class="container mt-4">
              <p>&copy; <script>document.write(new Date().getFullYear())</script> Desarrollado por <a>Francisco Jiménez</a> </p>
              <ul class="list-inline d-inline">
                <li class="list-inline-item mx-0"><a class="d-inline-block p-2 text-color" href="#"><i class="ti-facebook"></i></a></li>
                <li class="list-inline-item mx-0"><a class="d-inline-block p-2 text-color" href="#"><i class="ti-twitter-alt"></i></a></li>
                <li class="list-inline-item mx-0"><a class="d-inline-block p-2 text-color" href="#"><i class="ti-linkedin"></i></a></li>
                <li class="list-inline-item mx-0"><a class="d-inline-block p-2 text-color" href="#"><i class="ti-instagram"></i></a></li>
              </ul>
            </div>
          </footer>
        </div>

   
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-slim.min.js"><\/script>')</script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>

@endsection
